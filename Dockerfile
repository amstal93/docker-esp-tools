FROM python:alpine

RUN apk update && \
    apk add --no-cache curl jq && \
    pip install esptool && \
    curl https://raw.githubusercontent.com/esp8266/Arduino/master/tools/espota.py -o /usr/local/bin/espota.py && \
    chmod +x /usr/local/bin/espota.py && \
    curl https://raw.githubusercontent.com/homieiot/homie-esp8266/develop/scripts/ota_updater/ota_updater.py -o /usr/local/bin/homieota.py && \
    chmod +x /usr/local/bin/homieota.py && \
    curl https://raw.githubusercontent.com/homieiot/homie-esp8266/develop/scripts/ota_updater/requirements.txt -o /tmp/requirements.txt && \
    pip install -r /tmp/requirements.txt && \
    apk del curl